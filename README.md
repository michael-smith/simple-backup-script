# simple-backup-script
If the script is placed in the user's home directory (~/.backup_home.sh) and the executable flag `chmod +x ~/.backup_home.sh` is set, it can be executed with

    ./.backup_home.sh

to save specific files and directories to another, at best external, device.

The script will create two directories on the backup device. The first is the archive, that will contain all files that were ever saved -- but always the latest version of it. So if a file will just change its content but not the name, only the latest content is available in the archive. So a file with a constant name means overwrite the file in the archive.

The second folder is a mirror of the saved files and directories. When a file is deleted and the backup script runs, it will be also deleted in the mirror directory (but not in the archive).

## specifying files to backup in arrBackup
* To backup all files in a directory, write the name of the it followed by a slash, e.g. see line 8 in .backup_home.sh
* To backup a specific file at the same level as the .backup_home.sh file, add the name without a slash at the end, e.g. see line 18
* To backup all files in a subdirectory, add each path of the parent directories in a seperate line, followed by two slashes, e.g. see lines 18...20. A single slash at the end is needed for the actual directory to save. This will save everything in ```.local/share/applications/```.
* To backup a specific file in a subdirectory, follow the double slash convention like in the point above but in the line of interest, don't end it with a slash to indicate a directory but specify the file, e.g.

        ".config//"
        ".config/some_program//"
        ".config/some_program/config.cfg"
