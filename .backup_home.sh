#!/bin/bash

arrPth=("/run/media/michael/flashdrive/home/"
        "/run/media/michael/encrypted/home/"
        "/another/path/to/another/backup/device/home/")

arrBackup=("Desktop/"
           "Documents/"
           "Downloads/"
           "Music/"
           "Pictures/"
           "Projects/"
           "Public/"
           "Templates/"
           "Videos/"
           ".ssh/"
           ".backup_home.sh"
           ".local//"
           ".local/share//"
           ".local/share/applications/")

user=$(id -u -n)
arrSub=("$user""_archive/" "$user""_mirror/")
arrLog=("rsync_archive.log" "rsync_mirror.log")
arrArg=("--progress" "--progress --delete --delete-excluded")

# Check if max string length is min 512 chars for concating eight times sha256sum
if [ "$(getconf ARG_MAX)" -lt 512 ]; then
    echo "Maximal string length must be min 512 chars but is only $(getconf ARG_MAX)"
    exit 0
fi

# Display the menu to choose the directory where to save the backup from the list in arrPth
i=0
for p in "${arrPth[@]}"; do
    echo "$i - $p"
    i=$(($i + 1))
done
echo "q - quit"
read opt

# Check if the option is valid and set the right path as part of the destination for rsync
pth=""
i=0
if [ "$opt" != "q" ]; then
    for p in "${arrPth[@]}"; do
        if [ "$opt" == "$i" ]; then
            pth="$p"
	    fRsync=1
	    break
        else
            i=$(($i + 1))
        fi
    done
    if [ "$pth" == "" ]; then
        echo "$opt is invalid"
        exit 0
    fi
else
    echo quit
    exit 0
fi

# Option to only check the sha256sums instead of performing rsync and sha256sum check
echo "Only show sha256sum?"
echo "0 - no"
echo "1 - yes"
read fSha

# Create include arguments, add "***" to directories, that rsync copies its content
# Create arguments for "find"
inc=()
arrBackupNoMeta=()
for item in "${arrBackup[@]}"; do
    if [ "${item: -2}" == "//" ]; then
        add="${item::-1}"
    elif [ "${item: -1}" == "/" ]; then
        add="$item""***"
        arrBackupNoMeta+=("$item")
    else
        add="$item"
        arrBackupNoMeta+=("$item")
    fi
    inc+=("--include=$add")
done

if [ "$fSha" == "0" ]; then
    i=0
    for sub in "${arrSub[@]}"; do
        rsync -avh "${inc[@]}" --exclude=* ${arrArg[$i]} --log-file="${arrLog[$i]}" "/home/""$user""/" "$pth""$sub"
        i=$(($i + 1))
    done
fi

# Check sha256sums for mirror directory
shaOfFiles() {
    sum=""
    i=0
    for name in "$@"; do
        if [ -f "$name" ]; then
            sum="$sum"$(sha256sum "$name" | cut -d " " -f 1)
	    i=$((i + 1))
        fi
        if [ "$i" == "7" ]; then
            sum=$(echo "$sum" | sha256sum | cut -d " " -f 1) # limit chars of sum to 512
            i=0
        fi
    done
    echo $(echo "$sum" | sha256sum | cut -d " " -f 1)
}

readarray -d "" arrOrg < <(find "${arrBackupNoMeta[@]}" -print0 | sort -z)    # get all files based on the backuplist arrBackup
arrArc=()                                                               # all files to backup should be checked in the archive, if they were backuped and valid
for name in "${arrOrg[@]}"; do
    arrArc+=("$pth""${arrSub[0]}""$name")
done
readarray -d "" arrMir < <(find "$pth""${arrSub[1]}" -print0 | sort -z) # find all files in the mirror directory, because there should be just the files that were backuped and nothing more (no old files)

sumOrg=$(shaOfFiles "${arrOrg[@]}")
sumMir=$(shaOfFiles "${arrMir[@]}")
sumArc=$(shaOfFiles "${arrArc[@]}")
echo "sha256sum of original: $sumOrg"
echo "sha256sum of archive:  $sumArc"
echo "sha256sum of mirror:   $sumMir"
if [ "$sumOrg" == "$sumMir" ] && [ "$sumOrg" == "$sumArc" ]; then
    msg="Checksums are equal."
else
    msg="ATTENTION: Checksums do NOT match."
fi
echo "$msg"
